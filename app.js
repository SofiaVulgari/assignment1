const balanceElement = document.getElementById("balance");
const outstandingElement = document.getElementById("outstanding");
const getLoanElement = document.getElementById("loan");
const payElement = document.getElementById("pay");
const repayButtonElement = document.getElementById("repayButton");
const bankButtonElement = document.getElementById("bankButton");
const workButtonElement = document.getElementById("workButton");
const productsElement = document.getElementById("products");
const featuresElement = document.getElementById("features");
const imageElement = document.getElementById("image");
const productNameElement = document.getElementById("productName");
const productCostElement = document.getElementById("productCost");
const productDescriptionElement = document.getElementById("productDescription");
const buyElement = document.getElementById("buyButton");

/**
 * Initial variable for the balance, the outstanding loan, the salary/pay, and an empty array for the products.
 */
let balance = 200;
let outstanding = 0;
let pay = 0;
let products = [];

/**
 * Function that handles the getting a loan action.
 * If the loan is less or equal to the doudle of the current balance and if there is no other loan 
 * (so no outstanding loan), a loan can be taken.
 * If there is a loan the repay button appears and can be used.
 */
const handleGetLoan = () => { 
    if (outstanding === 0) {
        const askLoan = prompt("Please enter the ammount of money you wish to loan: ");
        const loan = parseFloat(askLoan);    
        if (loan <= balance*2) {
            balance += loan;
            outstanding = loan;
            balanceElement.innerHTML = `Balance ${balance} kr`;
            outstandingElement.innerHTML = `Outstanding Loan ${loan} kr`;
                if (loan > 0 ) {
                    repayButtonElement.removeAttribute("hidden");
                } else repayButtonElement.addAttribute("hidden");

            } else alert(`You cannot take a loan more than double of your current balance`); 
    } else alert(`You cannot take an other loan`);
}

/**
 * A function that handles adding money from working. 
 * Adds 100 kr per click.
 */
const handlePayMoney = () => {
    const workedMoney = 100;
    pay += workedMoney;
    payElement.innerHTML = `Pay ${pay} kr`;

}

/**
 * A function that handles the repayment action. If there is a loan bigger or equal 
 * to the amount to be payed then all of it goes to repay as much of the loan as possible. 
 * Else if the loan is a smaller amount, 
 * it is repayed by full and then the rest of the money are added to the bank balance.
 */
const handleRepay = () => {
   if (outstanding >= pay){
       outstanding -= pay;
       outstandingElement.innerHTML = `Outstanding Loan ${outstanding} kr`;
       payElement.innerHTML = `Pay ${pay} kr`;
   } else { 
      balance += pay - outstanding;   
      outstanding = 0;
      pay = 0;
      outstandingElement.innerHTML = `Outstanding Loan ${outstanding} kr`;
      balanceElement.innerHTML = `Balance ${balance} kr`;
      payElement.innerHTML = `Pay ${pay} kr`;
   }
}

/**
 * A function that handles the transfering money to the bank. If there is worked money and if there is no loan to be payed, 
 * all the money can be transferred to the bank balance. If there is a loan 10% of the worked money go to repay it 
 * (outstanding loan) and the rest 90% to the bank balance.
 */
const handleTransferMoney = () => {
    if (pay > 0) {
        if (outstanding <= 0) {
            balance += pay; 
            pay = 0;
            balanceElement.innerHTML = `Balance ${balance} kr`;
            payElement.innerHTML = `Pay ${pay} kr`;
        } else {
            outstanding -= 0.10 * pay;
            balance += 0.90 * pay;
            pay = 0;
            balanceElement.innerHTML = `Balance ${balance} kr`;
            outstandingElement.innerHTML  = `Outstanding Loan ${outstanding} kr`;
            payElement.innerHTML = `Pay ${pay} kr`;
        }
    } else alert("You need to work for some money first in order to send them to your bank.")
}

/**
 * fetches the json data with the laptops and their attributes from the noroff-komputer-store-api
 */
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => products = data)
    .then(products => addProducts(products));

/**
 * Function that adds the products(from the next function). It iterates through the laptops and adds/shows the product's specs, price, title, description and the image.  
 * @param {*} products
 */
const addProducts = (products) => {
    products.forEach(x => addProduct(x));
    featuresElement.innerText = products[0].specs;
    productCostElement.innerText = `${products[0].price} kr`;
    productNameElement.innerText = products[0].title;
    productDescriptionElement.innerText = products[0].description;
    image.setAttribute("src", "https://noroff-komputer-store-api.herokuapp.com/assets/images/1.png");

}

/**
 * Function that adds each product to the option menu 
 * @param {*} product 
 */
const addProduct = (product) => {
    const productElement = document.createElement("option");
    productElement.value = product.id;
    productElement.appendChild(document.createTextNode(product.title));
    productsElement.appendChild(productElement);
}

/**
 * Function that handles the change of products. When a specific (target) product is selected, the 
 * price, specs, title, description and image, change according to that.
 * The image for the 5th laptop was edited to png instead of jpg in order to work. 
 * @param {*} e 
 */
const handleFeaturesChange = e => {
    const selectedProduct = products[e.target.selectedIndex]; 
    productCostElement.innerText = selectedProduct.price;
    featuresElement.innerText = selectedProduct.specs;
    productNameElement.innerText = selectedProduct.title;
    productDescriptionElement.innerText = selectedProduct.description;
    if (selectedProduct.id == 5) {
        image.setAttribute('src', "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png")
    } else {
    const mainurl = "https://noroff-komputer-store-api.herokuapp.com/";
    image.setAttribute('src', `${mainurl}${selectedProduct.image}`);
    }
}

/**
 * Function that handles bying the laptop. Pop ups are shown for if the order was succesfull 
 * or if there were not enough funds to buy it.
 */
const handleBuyLaptop = () => {
    if (balance >= productCostElement.innerText) {
        balance -= productCostElement.innerText;
        balanceElement.innerHTML = `Balance ${balance} kr`;
        alert("Congratulations, your order has been made!");
    } else alert ("You do not have enough funds in your balance.")
}

/**
 * The event listeners for each button and the option change
 */
productsElement.addEventListener("change", handleFeaturesChange);
getLoanElement.addEventListener("click", handleGetLoan);
workButtonElement.addEventListener("click", handlePayMoney);
bankButtonElement.addEventListener("click", handleTransferMoney);
repayButtonElement.addEventListener("click", handleRepay);
buyElement.addEventListener("click", handleBuyLaptop);
